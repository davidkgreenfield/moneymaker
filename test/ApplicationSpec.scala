import org.scalatestplus.play._
import play.api.test._
import play.api.test.Helpers._

/**
 * Add your spec here.
 * You can mock out a whole application including requests, plugins etc.
 * For more information, consult the wiki.
 */
class ApplicationSpec extends PlaySpec with OneAppPerTest {

  "Routes" should {

    "send 404 on a bad request" in  {
      route(app, FakeRequest(GET, "/boum")).map(status(_)) mustBe Some(NOT_FOUND)
    }

  }

  "MoneyController" should {

    "return a coin map" in {
      val coinResult = route(app, FakeRequest(GET, "/coins/1.76")).get

      status(coinResult) mustBe OK
      contentType(coinResult) mustBe Some("application/json")
      val d = (contentAsJson(coinResult) \ "dollar")
      d.get.as[BigDecimal] mustEqual(BigDecimal(1))
    }
    "return a failure when input is less than zero" in {
      val coinResult = route(app, FakeRequest(GET, "/coins/-1.76")).get

      status(coinResult) mustBe BAD_REQUEST

    }    

  }
}
