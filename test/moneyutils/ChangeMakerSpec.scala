package moneyutils

import org.scalatestplus.play.PlaySpec

class ChangeMakerSpec extends PlaySpec{
  "ChangeMaker" should {
    "return a map of coin name to coin count" in {
      val changeMaker = new ChangeMaker()
      val results = changeMaker.prettify(BigDecimal(1.77))
      results.get("dollar").map(_ mustEqual(BigDecimal(1)))
      results.get("half dollar").map(_ mustEqual(BigDecimal(1)))
      results.get("quarter").map(_ mustEqual(BigDecimal(1)))
      results.get("penny").map(_ mustEqual(BigDecimal(2)))
    }
    "return a map of coin name to coin count with 0.0 input" in {
      val changeMaker = new ChangeMaker()
      val results = changeMaker.prettify(BigDecimal(0.0))
      results.size mustEqual 0
    }
  }
}