package moneyutils

import javax.inject.Inject
import scala.collection.mutable.Buffer

object ChangeMaker {
  
  val amounts = List(1.00,.50,.25,.10,.05,.01)
  
  val coins = scala.collection.Map[BigDecimal,String](
    BigDecimal(1.00) -> "dollar",
    BigDecimal(.50) -> "half dollar",
    BigDecimal(.25) -> "quarter",
    BigDecimal(.10) -> "dime",
    BigDecimal(.05) -> "nickel",
    BigDecimal(.01) -> "penny"
  )
}

class ChangeMaker @Inject()()  {
  import ChangeMaker._

  def myCalc(myInput: BigDecimal) : scala.collection.mutable.Buffer[BigDecimal]= {
    val coinCollection = scala.collection.mutable.Buffer[BigDecimal]()
    
    def calc(inputAmount: BigDecimal): Unit = {
      if(inputAmount > 0){
        val coin = amounts.filter(x => {x <= inputAmount}).take(1)(0)        
        coinCollection += coin
        calc(inputAmount - coin)
      }    
    }
    calc(myInput)
    coinCollection
  }
  
  def prettify(myInput: BigDecimal) : Map[String,Int] = {
    val result = myCalc(myInput)
    result.groupBy(identity).map{case(k, v) => (coins.get(k).getOrElse("?") -> v.size)}
  }
  
  
}