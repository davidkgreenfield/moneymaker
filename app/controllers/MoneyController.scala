package controllers

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import moneyutils.ChangeMaker

import javax.inject._
import play.api._
import play.api.mvc._
import play.api.libs.json.Json


@Singleton
class MoneyController @Inject()(changeMaker: ChangeMaker) extends Controller {

  def index = Action {
    Ok("We're going to need some information first")
  }
  
  def correctChange(amount:BigDecimal) = Action.async{ request =>
    
    if(amount >= BigDecimal(0.0)){
      val changeResult = changeMaker.prettify(amount)    
    
      Future(Ok(Json.toJson(changeResult)))
    }else{
      Future(BadRequest("Amount must be greater than zero"))
    }
  }

}
